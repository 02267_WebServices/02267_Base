## 02267 Base

This is the big one boys!

![Related image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSH5LYHgnH8kBgtnVzjjmKjmwFYHzqsgNiyX0CrxiWkvRUdR9Py&s)

### Quickstart

#### Install

```
docker-compose up -d
```

#### Run your tests

```
mvn clean test
```

### Dependencies

- *-controller images that needs to be built from each of the 02267_*Controller repos

