Feature: Generate transactions report for customers

  Scenario: Generate monthly report for customer with transaction
    Given a customer with transactions in last month
    When asked to generate a monthly report for customer
    Then the customer should receive a monthly report

  Scenario: Generate report for customer in period: last week
    Given a customer with transactions in last week
    When asked to generate a weekly report for customer
    Then the customer should receive a weekly report

  Scenario: Customer with no transactions tries to generate a report
    Given a customer with no transactions
    When asked to generate a monthly report for customer
    Then the customer should not get a report
