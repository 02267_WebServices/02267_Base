Feature: Generate transactions report for manager

  Scenario: Generate monthly report for manager with transaction
    Given a customer with transactions in last month
    And a merchant
    When asked to generate a monthly report for manager
    Then the manager should receive a monthly report

