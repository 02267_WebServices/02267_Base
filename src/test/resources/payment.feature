Feature: Payment feature

  Scenario: Simple payment
    Given a registered customer with a bank account
    And a registered merchant with a bank account
    And the customer has one unused token
    When requests payment for 100 kroner using the token
    Then the payment succeeds