Feature: Can a user refund a payment?
  The user needs to be able to refund a payment

  Scenario: The user can refund a payment
    Given a user with a completed payment
    When the user asks for a refund
    Then the user should get a refund
