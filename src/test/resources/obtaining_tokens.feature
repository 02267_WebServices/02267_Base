Feature: Obtaining unique tokens feature

  Scenario: Obtain new tokens given no unused tokens
    Given a registered customer with no unused tokens
    When the customer requests 5 new tokens
    Then the customer gets 5 unique tokens

  Scenario: Get denied when requesting too many tokens given no unused tokens
    Given a registered customer with 1 or no unused tokens
    When the customer requests 6 new tokens
    Then the customer's request should be denied

  Scenario: Obtain new tokens given the customer has 1 unused token
    Given a registered customer with 1 unused tokens
    When the customer requests 2 new tokens
    Then the customer gets 2 new tokens, and thus has a total of 3 tokens

  Scenario: Obtain new tokens given the customer has more than 1 unused token
    Given a registered customer with more than 1 unused tokens
    When the customer requests 2 new tokens
    Then the customer is rejected