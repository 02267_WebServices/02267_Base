Feature: Manage users

  Scenario: Create and delete customer
    Given the name "Alice" "Al", a cpr-number "231212-0002", a bank account ID "coolBankId1"
    When trying to create a new customer with this information
    And trying to delete the user aftwards
    Then the customer should be created
    And afterwards be deleted

  Scenario: Create and delete merchant
    Given the name "Bob Bu", a cpr-number "231212-0001", a bank account ID "coolBankId2"
    When trying to create a new merchant with this information
    And trying to delete the merchant aftwards
    Then the merchant should be created
    And aftwards be deleted
