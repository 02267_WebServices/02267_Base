Feature: Generate transactions report for merchant

  Scenario: Generate monthly report for merchant with transaction
    Given a merchant with transactions in last month
    When asked to generate a monthly report for merchant
    Then the merchant should receive a monthly report

  Scenario: Generate report for merchant in period: last week
    Given a merchant with transactions in last week
    When asked to generate a weekly report for merchant
    Then the merchant should receive a weekly report

  Scenario: Merchant with no transactions tries to generate a report
    Given a merchant with no transactions
    When asked to generate a monthly report for merchant
    Then the merchant should not get a report
