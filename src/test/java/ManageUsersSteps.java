import static org.junit.Assert.assertEquals;

import javax.json.Json;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ManageUsersSteps {
  private Response createResult;
  private Response deleteResult;
  private String firstName;
  private String lastName;
  private String cpr;
  private String bankId;
  private Client client = ClientBuilder.newClient();
  private WebTarget baseUrl = client.target("http://localhost/");
  private String customerId;
  private String merchantId;

  @Given("the name {string} {string}, a cpr-number {string}, a bank account ID {string}")
  public void the_name_a_cpr_number_a_bank_account_ID(String firstName, String lastName, String cpr, String bankId) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.cpr = cpr;
    this.bankId = bankId;
  }

  @When("trying to create a new customer with this information")
  public void trying_to_create_a_new_customer_with_this_information() {
    String payload = Json.createObjectBuilder()
        .add("firstName", firstName)
        .add("lastName", lastName)
        .add("cpr", cpr)
        .add("bankAccountId", bankId)
        .build()
        .toString();
    createResult = baseUrl
        .path("c")
        .path("customer")
        .request()
        .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
    customerId = createResult.readEntity(String.class);
  }

  @When("trying to delete the user aftwards")
  public void trying_to_delete_the_user_aftwards() {
    deleteResult = baseUrl
        .path("c")
        .path("customer")
        .queryParam("id", customerId)
        .request()
        .delete();
  }

  @Then("the customer should be created")
  public void the_customer_should_be_created() {
    int expectedResponseCode = 201;
    assertEquals(expectedResponseCode, createResult.getStatus());
  }

  @Then("afterwards be deleted")
  public void afterwards_be_deleted() {
    int expectedResponseCode = 200;
    assertEquals(expectedResponseCode, deleteResult.getStatus());
  }

  @Given("the name {string}, a cpr-number {string}, a bank account ID {string}")
  public void the_name_a_cpr_number_a_bank_account_ID(String name, String cpr, String bankId) {
    this.firstName = name;
    this.cpr = cpr;
    this.bankId = bankId;
  }

  @When("trying to create a new merchant with this information")
  public void trying_to_create_a_new_merchant_with_this_information() {
    String payload = Json.createObjectBuilder()
        .add("name", firstName)
        .add("cpr", cpr)
        .add("bankAccountId", bankId)
        .build()
        .toString();
    createResult = baseUrl
        .path("m")
        .path("merchant")
        .request()
        .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
    merchantId = createResult.readEntity(String.class);
  }

  @When("trying to delete the merchant aftwards")
  public void trying_to_delete_the_merchant_aftwards() {
    deleteResult = baseUrl
        .path("m")
        .path("merchant")
        .queryParam("id", merchantId)
        .request()
        .delete();
  }

  @Then("the merchant should be created")
  public void the_merchant_should_be_created() {
    int expectedResponseCode = 201;
    assertEquals(expectedResponseCode, createResult.getStatus());
  }

  @Then("aftwards be deleted")
  public void aftwards_be_deleted() {
    int expectedResponseCode = 204;
    assertEquals(expectedResponseCode, deleteResult.getStatus());
  }
}
