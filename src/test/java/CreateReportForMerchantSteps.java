import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class CreateReportForMerchantSteps {
    static Response result;
    static String merchantId;
    static LocalDate period;

    Client client = ClientBuilder.newClient();;
    WebTarget baseUrl = client.target("http://localhost/");

    @Given("a merchant with transactions in last month")
    public void a_merchant_with_transactions_in_last_month() {
        merchantId = "1";
        period = LocalDate.now().minusMonths(1);
    }

    @When("asked to generate a monthly report for merchant")
    public void asked_to_generate_a_monthly_report_for_merchant() {
        result = baseUrl.path("t")
                .path("report")
                .queryParam("cpr", merchantId)
                .queryParam("period", period.toString())
                .request()
                .get();
    }

    @Then("the merchant should receive a monthly report")
    public void the_merchant_should_receive_a_monthly_report() {
        assertFalse(result.readEntity(String.class).isEmpty());
    }

    @Given("a merchant with transactions in last week")
    public void a_merchant_with_transactions_in_last_week() {
        merchantId = "2";
        period = LocalDate.now().minusWeeks(1);

        client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost/");
    }

    @When("asked to generate a weekly report for merchant")
    public void asked_to_generate_a_weekly_report_for_merchant() {
        result = baseUrl.path("t")
                .path("report")
                .queryParam("cpr", merchantId)
                .queryParam("period", period.toString())
                .request()
                .get();
    }

    @Then("the merchant should receive a weekly report")
    public void the_merchant_should_receive_a_weekly_report() {
        assertEquals(200, result.getStatus());
        assertFalse(result.readEntity(String.class).isEmpty());
    }

    @Given("a merchant with no transactions")
    public void a_merchant_with_no_transactions() {
        merchantId = "3";
        period = LocalDate.now().minusMonths(1);

        client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost/");
    }

    @Then("the merchant should not get a report")
    public void the_merchant_should_not_get_a_report() {
        assertTrue(result.readEntity(String.class).isEmpty());
    }
}
