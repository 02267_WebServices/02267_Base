import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.json.Json;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RefundPaymentSteps {
    Response result;
    Integer amount = -200;
    List<String> tokens;

    Client client = ClientBuilder.newClient();
    WebTarget baseUrl = client.target("http://localhost/");

    @Given("a user with a completed payment")
    public void a_user_with_a_completed_payment() {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String customerId;
        String firstName = "yaetfddsajodsakjl";
        String lastName = "yeatdsakjsa";
        String cpr = "cprdsakjdjsal";

        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", 1)
                .request()
                .get();
    }

    @When("the user asks for a refund")
    public void the_user_asks_for_a_refund() {
        tokens = result.readEntity(List.class);
        String payload = Json.createObjectBuilder()
                .add("name", "Johnany2")
                .add("cpr", "BigFacae2")
                .add("bankAccountId", BankAccounts.merchantBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("m")
                .path("merchant")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        String debtorId = result.readEntity(String.class);
        String description = "refund";
        String tokenId = tokens.get(0);
        payload = Json.createObjectBuilder()
                .add("debtorId", debtorId)
                .add("description", description)
                .add("tokenId", tokenId)
                .add("amount", amount)
                .build().toString();

        result = baseUrl.path("m")
                .path("transaction")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
    }

    @Then("the user should get a refund")
    public void the_user_should_get_a_refund() {
        assertEquals(200, result.getStatus());
    }
}
