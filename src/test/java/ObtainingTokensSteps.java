import gherkin.deps.com.google.gson.Gson;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.json.Json;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.*;

public class ObtainingTokensSteps {
    Response result;
    String customerId;
    Client client = ClientBuilder.newClient();
    WebTarget baseUrl = client.target("http://localhost/");

    @Given("a registered customer with no unused tokens")
    public void a_registered_customer_with_no_unused_tokens() {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String firstName = "Sebdsaewqe";
        String lastName = "Hopewdsaq";
        String cpr = "cprkjdnksadsad";
        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build().toString();

        result = baseUrl.path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);
    }

    @When("the customer requests {int} new tokens")
    public void the_customer_requests_new_tokens(Integer int1) {
        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", int1)
                .request()
                .get();
    }

    @Then("the customer gets {int} unique tokens")
    public void the_customer_gets_unique_tokens(Integer int1) {
        List<String> tokens = new Gson().fromJson(result.readEntity(String.class), List.class);

        assertEquals((int) int1, tokens.size());
    }

    @Given("a registered customer with {int} or no unused tokens")
    public void a_registered_customer_with_or_no_unused_tokens(Integer int1) {
            baseUrl.path("t")
                    .path("hello")
                    .request()
                    .get();

            String firstName = "Sebdsaewqe";
            String lastName = "Hopewdsaq";
            String cpr = "cprkjdnksadsad";
            String payload = Json.createObjectBuilder()
                    .add("firstName", firstName)
                    .add("lastName", lastName)
                    .add("cpr", cpr)
                    .add("bankAccountId", BankAccounts.customerBankAccount)
                    .build().toString();

            result = baseUrl.path("c")
                    .path("customer")
                    .request()
                    .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
            customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", int1)
                .request()
                .get();
    }

    @Then("the customer's request should be denied")
    public void the_customer_s_request_should_be_denied() {

        assertFalse(200 == result.getStatus());
    }

    @Given("a registered customer with {int} unused tokens")
    public void a_registered_customer_with_unused_tokens(Integer int1) {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String firstName = "Sebdsaewqe";
        String lastName = "Hopewdsaq";
        String cpr = "cprkjdnksadsad";
        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build().toString();

        result = baseUrl.path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", int1)
                .request()
                .get();
    }

    @Then("the customer gets {int} new tokens, and thus has a total of {int} tokens")
    public void the_customer_gets_new_tokens_and_thus_has_a_total_of_tokens(Integer int1, Integer int2) {
        List<String> tokens = new Gson().fromJson(result.readEntity(String.class), List.class);

        assertEquals((int) int2, tokens.size());
    }

    @Given("a registered customer with more than {int} unused tokens")
    public void a_registered_customer_with_more_than_unused_tokens(Integer int1) {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String firstName = "Sebdsaewqe";
        String lastName = "Hopewdsaq";
        String cpr = "cprkjdnksadsad";
        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build().toString();

        result = baseUrl.path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", int1+1)
                .request()
                .get();
    }

    @Then("the customer is rejected")
    public void the_customer_is_rejected() {
        assertFalse(200 == result.getStatus());
    }
}