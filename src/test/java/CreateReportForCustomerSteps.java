import dtu.ws.fastmoney.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.json.Json;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class CreateReportForCustomerSteps {
    Response result;
    String customerId;
    LocalDate period;

    Client client = ClientBuilder.newClient();;
    WebTarget baseUrl = client.target("http://localhost/");

    @Given("a customer with transactions in last month")
    public void a_customer_with_transactions_in_last_month() {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String firstName = "yaetdsa";
        String lastName = "yeatdsa";
        String cpr = "cpdsar";

        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", 1)
                .request()
                .get();

        List<String> tokens = result.readEntity(List.class);

        payload = Json.createObjectBuilder()
                .add("name", "Johndsny")
                .add("cpr", "BigFadsce")
                .add("bankAccountId", BankAccounts.merchantBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("m")
                .path("merchant")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        String debtorId = result.readEntity(String.class);
        String description = "trans";
        String tokenId = tokens.get(0);
        BigDecimal amount = new BigDecimal(200);
        payload = Json.createObjectBuilder()
                .add("debtorId", debtorId)
                .add("description", description)
                .add("tokenId", tokenId)
                .add("amount", amount)
                .build().toString();

        result = baseUrl.path("m")
                .path("transaction")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        period = LocalDate.now().minusMonths(1);
    }

    @When("asked to generate a monthly report for customer")
    public void asked_to_generate_a_monthly_report_for_customer() {
        result = baseUrl.path("t")
                .path("report")
                .queryParam("cpr", customerId)
                .queryParam("period", period.toString())
                .request()
                .get();
    }

    @Then("the customer should receive a monthly report")
    public void the_customer_should_receive_a_monthly_report() {
        assertEquals(200, result.getStatus());
        assertFalse(result.readEntity(String.class).isEmpty());
    }

    @Given("a customer with transactions in last week")
    public void a_customer_with_transactions_in_last_week() {
        baseUrl.path("t")
                .path("hello")
                .request()
                .get();

        String firstName = "yaeewaadwt";
        String lastName = "yedawaat";
        String cpr = "cpraaaa";

        String payload = Json.createObjectBuilder()
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("cpr", cpr)
                .add("bankAccountId", BankAccounts.customerBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("c")
                .path("customer")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
        customerId = result.readEntity(String.class);

        result = baseUrl.path("c")
                .path("tokens")
                .queryParam("id", customerId)
                .queryParam("amount", 1)
                .request()
                .get();

        List<String> tokens = result.readEntity(List.class);

        payload = Json.createObjectBuilder()
                .add("name", "Johnany2")
                .add("cpr", "BigFacae2")
                .add("bankAccountId", BankAccounts.merchantBankAccount)
                .build()
                .toString();
        result = baseUrl
                .path("m")
                .path("merchant")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        String debtorId = result.readEntity(String.class);
        String description = "tranny";
        String tokenId = tokens.get(0);
        Integer amount = 200;
        payload = Json.createObjectBuilder()
                .add("debtorId", debtorId)
                .add("description", description)
                .add("tokenId", tokenId)
                .add("amount", amount)
                .build().toString();

        result = baseUrl.path("m")
                .path("transaction")
                .request()
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        period = LocalDate.now().minusWeeks(1);
    }

    @When("asked to generate a weekly report for customer")
    public void asked_to_generate_a_weekly_report_for_customer() {
        result = baseUrl.path("t")
                .path("report")
                .queryParam("cpr", customerId)
                .queryParam("period", period.toString())
                .request()
                .get();
    }

    @Then("the customer should receive a weekly report")
    public void the_customer_should_receive_a_weekly_report() {
        assertEquals(200, result.getStatus());
        assertFalse(result.readEntity(String.class).isEmpty());
    }

    @Given("a customer with no transactions")
    public void a_customer_with_no_transactions() {
        customerId = "3";
        period = LocalDate.now().minusMonths(1);
    }

    @Then("the customer should not get a report")
    public void the_customer_should_not_get_a_report() {
        assertEquals(200, result.getStatus());
        assertTrue(result.readEntity(String.class).isEmpty());
    }
}
