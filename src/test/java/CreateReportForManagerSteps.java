import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class CreateReportForManagerSteps {

    Client client = ClientBuilder.newClient();
    WebTarget baseUrl = client.target("http://localhost/");

    Response result;
    String managerId;
    LocalDate period = LocalDate.now().minusMonths(1);

    @Given("a merchant")
    public void a_merchant() {
        managerId = "1234-1234-1234-1234";
    }

    @When("asked to generate a monthly report for manager")
    public void asked_to_generate_a_monthly_report_for_manager() {
        result = baseUrl.path("t")
                .path("report")
                .queryParam("cpr", managerId)
                .queryParam("period", period.toString())
                .request()
                .get();
    }

    @Then("the manager should receive a monthly report")
    public void the_manager_should_receive_a_monthly_report() {
        assertEquals(200, result.getStatus());
        assertFalse(result.readEntity(String.class).isEmpty());
    }

}
