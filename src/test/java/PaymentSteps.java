import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.json.Json;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PaymentSteps {
  Response result;
  String customerId;
  String merchantId;
  
  Client client = ClientBuilder.newClient();
  WebTarget baseUrl = client.target("http://localhost/");

  @Given("a registered customer with a bank account")
  public void a_registered_customer_with_a_bank_account() {
    baseUrl.path("t")
            .path("hello")
            .request()
            .get();

    String firstName = "Sebdsaewqe";
    String lastName = "Hopewdsaq";
    String cpr = "cprkjdnksadsad";
    String payload = Json.createObjectBuilder()
            .add("firstName", firstName)
            .add("lastName", lastName)
            .add("cpr", cpr)
            .add("bankAccountId", BankAccounts.customerBankAccount)
            .build().toString();

    result = baseUrl.path("c")
            .path("customer")
            .request()
            .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
    customerId = result.readEntity(String.class);
  }

  @Given("a registered merchant with a bank account")
  public void a_registered_merchant_with_a_bank_account() {
    baseUrl.path("t")
            .path("hello")
            .request()
            .get();

    String name = "mercdsadh";
    String cpr = "merchantcpr";
    String payload = Json.createObjectBuilder()
            .add("name", name)
            .add("cpr", cpr)
            .add("bankAccountId", BankAccounts.merchantBankAccount)
            .build().toString();

    result = baseUrl.path("m")
            .path("merchant")
            .request()
            .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
    merchantId = result.readEntity(String.class);
  }

  @Given("the customer has one unused token")
  public void the_customer_has_one_unused_token() {
    int amount = 1;

    result = baseUrl.path("c")
            .path("tokens")
            .queryParam("id", customerId)
            .queryParam("amount", amount)
            .request()
            .get();
  }

  @When("requests payment for {int} kroner using the token")
  public void requests_payment_for_kroner_using_the_token(Integer int1) {
    List<String> tokens = result.readEntity(List.class);
    String token = tokens.get(0);
    String payload = Json.createObjectBuilder()
            .add("debtorId", merchantId)
            .add("description", "description")
            .add("tokenId", token)
            .add("amount", int1)
            .build().toString();

    result = baseUrl.path("m")
                        .path("transaction")
                        .request()
                        .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
  }

  @Then("the payment succeeds")
  public void the_payment_succeeds() {
    assertEquals(200, result.getStatus());
  }
}
